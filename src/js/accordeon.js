export default function AccordeonGroup(buttonSelector) {
  const buttons = [...document.querySelectorAll(buttonSelector)];
  const pairs = buttons.map(button => {
    const targetId = button.dataset.accordeonTarget;
    const target = document.getElementById(targetId);
    return { button: button, target: target };
  });
  pairs.forEach(clicked => {
    clicked.button.addEventListener("click", () => {
      pairs.forEach(current => {
        if (current === clicked) {
          current.target.classList.remove("hidden");
        } else {
          current.target.classList.add("hidden");
        }
      });
    });
  }); 
}
