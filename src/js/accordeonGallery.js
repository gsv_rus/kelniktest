function AccordeonGallery(container, imageSelector) {
  if (!container) return;
  const images = container.querySelectorAll(imageSelector);
  if (images.length <= 8) return;

  let state = { isCollapsed: true };

  const buttonShow = createButton();
  render();

  function createButton() {
    const buttonShow = document.createElement("button");
    container.appendChild(buttonShow);
    buttonShow.classList.add("show-more");
    buttonShow.classList.add("show-more--down");
    const arrow = document.createElement("div");
    container.appendChild(arrow);
    arrow.classList.add("arrow");
    buttonShow.addEventListener("click", handleClick);
    return buttonShow;
  }

  function handleClick() {
    state.isCollapsed = !state.isCollapsed;
    render();
  }

  function render() {
    buttonShow.textContent = state.isCollapsed ? "Показать еще" : "Свернуть";
    buttonShow.classList.add(state.isCollapsed ? "show-more--down" : "show-more--up");
    buttonShow.classList.remove(state.isCollapsed ? "show-more--up" : "show-more--down");
    [...images].forEach((elem, i) => {
      if (i >= 8 && state.isCollapsed) {
        elem.classList.add("hidden");
      } else {
        elem.classList.remove("hidden");
      }
    });
  }
}

export default AccordeonGallery;
