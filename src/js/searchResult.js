export default function searchResult() {
  if (
    location.pathname !== "/search.html" &&
    location.pathname !== "/rezultatyi-poiska/"
  )
    return;
  const inputElement = document.querySelector(".search__input");
  inputElement.value = getSearchQuery();

  function getSearchQuery() {
    let valueUrl = decodeURI(window.location.search);
    if (!valueUrl) return " ";
    return valueUrl
      .split("?")[1]
      .split("&")
      .map(param => {
        return { name: param.split("=")[0], value: param.split("=")[1] };
      })
      .find(param => param.name === "query")
      .value.replace(/\+/g, " ")
      .replace(/\%2B/g, "+");
  }
}
