export default function hideOnScroll(element, treshold = 30) {
  document.addEventListener("scroll", () => {
    if (window.scrollY > treshold) {
      element.style.opacity = "0";
    }
    if (window.scrollY < treshold) {
      element.style.opacity = "1";
    }
  });
}
