export default function Tabulator(app, scrollPages) {
  let state = { isCollapsed: false };
  scrollPages.forEach(element => {
    if (location.pathname === element) state.isCollapsed = true;
  });
  const tabButtons = [...document.querySelectorAll(".js-tab-button")].map(
    button => {
      const tabTarget = button.dataset.tabTarget;
      if (typeof tabTarget === "undefined") return;
      button.addEventListener("click", () => showTab(tabTarget));
      return button;
    }
  );
  const availableIds = tabButtons.map(button => {
    return button.dataset.tabTarget;
  });

  if (tabButtons.length <= 1) return;
  if (location.hash && availableIds.includes(location.hash.replace("#", ""))) {
    showTab(location.hash.replace("#", ""));
  } else {
    showTab(tabButtons[0].dataset.tabTarget);
  }

  function showTab(target) {
    [...document.querySelectorAll(".js-tab")].forEach(tab => {
      if (tab.id === target) {
        tab.style.display = "";
      } else {
        tab.style.display = "none";
      }
    });
    [...document.querySelectorAll(".js-tab-button")].forEach(button => {
      if (button.dataset.tabTarget === target) {
        button.classList.add("js-tab-button--active");
        if (state) {
          window.scrollTo({
            top: button.offsetTop - 50,
            behavior: "smooth"
          });
        }
      } else {
        button.classList.remove("js-tab-button--active");
      }
    });
  }
}
